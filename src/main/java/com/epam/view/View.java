package com.epam.view;

import com.epam.Application;
import com.epam.controller.Controller;
import com.epam.controller.ControllerIml;
import com.epam.model.DroidsShip;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Class to connect with controller, this class just show the data.
 *
 * @version 2.1
 * Created by Borys Latyk on 13/11/2019.
 * @since 10.11.2019
 */
public class View {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    BufferedReader input = new BufferedReader
            (new InputStreamReader(System.in));


    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", ("1 - Serialize Droids"));
        menu.put("2", ("2 - Compare reading and writing"));
        menu.put("3", ("3 - Reading a Java source-code"));
        menu.put("4", ("4 - Contents of a specific directory"));
        menu.put("Q", ("Q - Exit"));
    }

    public View() {

        controller = new ControllerIml();
        setMenu();
        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("Q", this::pressButton5);

    }

    private void pressButton1() {
       logger1.info(controller.getdroids());
    }

    private void pressButton2() throws IOException {
        DroidsShip.reading();
    }
    private void pressButton3() throws IOException {
        controller.readcode();
    }

    private void pressButton4() throws IOException {
        controller.contents();
    }

    private void pressButton5() {
        logger1.info("Bye-Bye");
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        logger1.info("\nIO:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() throws Exception {
        String keyMenu;
        do {
            outputMenu();
            logger1.info("Please, select menu point");
            keyMenu = input.readLine().toUpperCase();
            methodsMenu.get(keyMenu).print();
        } while (!keyMenu.equals("Q"));
    }
}


