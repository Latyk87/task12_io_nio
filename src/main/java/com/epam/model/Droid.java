package com.epam.model;

import java.io.Serializable;
/**
 * Class creates describe Droids.
 *
 * @version 2.1
 * Created by Borys Latyk on 07/12/2019.
 * @since 07.12.2019
 */
public class Droid implements Serializable {
    private String name;
    private  String type;
    private int power;
    private int speed;
    private boolean warDroid;
    private transient int health;


    public Droid(String name, String type, int power, int speed, boolean warDroid, int health) {
        this.name = name;
        this.type = type;
        this.power = power;
        this.speed = speed;
        this.warDroid = warDroid;
        this.health = health;
    }

    @Override
    public String toString() {
        return "Droid{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", power=" + power +
                ", speed=" + speed +
                ", warDroid=" + warDroid +
                ", heatlh=" + health +
                '}';
    }
}