package com.epam.model;
import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.stream.Stream;
/**
 * Class is for IO Streams testing.
 *
 * @version 2.1
 * Created by Borys Latyk on 07/12/2019.
 * @since 07.12.2019
 */
public class SourceCode {
    private static Logger logger1 = LogManager.getLogger(Application.class);
    public void readCodeIml() throws IOException {
      FileInputStream inputStream =  new FileInputStream("E:/Epam knowledge/colectionspart1/src/main/java/com/epam/view/View.java");
        BufferedReader stream = new BufferedReader(new InputStreamReader(inputStream));
        String string;
            while((string=stream.readLine())!=null){
                Stream<String> stringStream=Stream.of(string);
                stringStream.filter(x -> x.startsWith(" *")).forEach(System.out::println);
            }

    }
public void contentsIml(){
File file =new File("E://Epam knowledge//colectionspart1");
if(file.exists()){
printContents(file,"");
}
}
private void printContents(File f,String s){
   logger1.info(s+"Directory " +f.getName());
   s=s+"  ";
   File [] filenames=f.listFiles();
    for (File fr:filenames) {
        if(fr.isDirectory()){
            printContents(fr,s);
        }else{
            logger1.info(s+"File "+fr.getName());
        }
    }
}

}