package com.epam.model;

import com.epam.Application;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Class for serialize and deserialize Droids.
 *
 * @version 2.1
 * Created by Borys Latyk on 07/12/2019.
 * @since 07.12.2019
 */
public class DroidsShip implements Model {
    List<Droid> unserialize = new ArrayList<>();
    private static Logger logger1 = LogManager.getLogger(Application.class);

    @Override
    public List<Droid> getdroids() {
        Droid d1 = new Droid("Ruby", "Support", 45, 8, false, 20);
        Droid d2 = new Droid("Shuriken", "Defence", 70, 5, true, 40);
        Droid d3 = new Droid("Falcon", "Explorer", 100, 15, true, 60);
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("droid.out"))) {

            objectOutputStream.writeObject(d1);
            objectOutputStream.writeObject(d2);
            objectOutputStream.writeObject(d3);
            logger1.info(d1);
            logger1.info(d2);
            logger1.info(d3);

        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("droid.out"))) {

            Droid ud1 = (Droid) objectInputStream.readObject();
            Droid ud2 = (Droid) objectInputStream.readObject();
            Droid ud3 = (Droid) objectInputStream.readObject();
            unserialize.add(ud1);
            unserialize.add(ud2);
            unserialize.add(ud3);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return unserialize;
    }

    public static void reading() throws IOException {
     try(InputStream stream=new FileInputStream("C:/Users/User/Desktop/LearnJava.pdf")){
        int data=stream.read();
        long startByte=System.currentTimeMillis();
        while(data!=-1){
            data=stream.read();
        }
     long startByteEnd=System.currentTimeMillis()-startByte;
        logger1.info(startByteEnd+" ms");
     }catch (Exception e) {
         e.printStackTrace();
     }
        int data=1*1024*1024;
        DataInputStream quickstream = new DataInputStream(new BufferedInputStream
                (new FileInputStream("C:/Users/User/Desktop/LearnJava.pdf"),data));
        long startbuffer=System.currentTimeMillis();
        try{
            while(true){
            long d=quickstream.readLong();
            }
        }catch(EOFException e){

        }
long startbufferend =System.currentTimeMillis()-startbuffer;
        logger1.info(startbufferend+" ms");

    }
}