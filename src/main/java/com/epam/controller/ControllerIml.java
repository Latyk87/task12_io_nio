package com.epam.controller;

import com.epam.model.Droid;
import com.epam.model.DroidsShip;
import com.epam.model.Model;
import com.epam.model.SourceCode;
import java.io.IOException;
import java.util.List;

/**
 * Class contains methods for proper working of controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public class ControllerIml implements Controller {

    Model link;

    @Override
    public List<Droid> getdroids() {
        link = new DroidsShip();
        return link.getdroids();
    }

    @Override
    public void readcode() throws IOException {
        SourceCode sourceCode = new SourceCode();
        sourceCode.readCodeIml();
    }

    @Override
    public void contents() throws IOException {
        SourceCode sourceCode2 = new SourceCode();
        sourceCode2.contentsIml();
    }
}
