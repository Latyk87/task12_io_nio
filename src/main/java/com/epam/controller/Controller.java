package com.epam.controller;

import com.epam.model.Droid;
import java.io.IOException;
import java.util.List;

/**
 * Interface for controller.
 * Created by Borys Latyk on 13/11/2019.
 *
 * @version 2.1
 * @since 10.11.2019
 */
public interface Controller {
List<Droid> getdroids();
void readcode() throws IOException;
void contents() throws IOException;
}
